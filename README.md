# safe-modular-arithmetic

Implementation of modular arithmetic algorithms for all integer types in an overflow-safe and const-compatible manner.